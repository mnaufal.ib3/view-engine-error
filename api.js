const express = require("express");
const router = express.Router();

const user = [
    {
        id: 1,
        email: "admin@app.com"
    },
    {
        id: 2,
        email: "user@app.com"
    }
]

const timeLog = (req, res, next) => {
    console.log(`${req.url} get accessed at ${Date.now()}`)
    next()
}

router.use(timeLog)

router.get("/", (req, res) => {
    res.json(user)
})

router.get("/userCount", (req, res) => {
    res.json({userCount: user.length})
})

router.get("/result", (req, res) => {
    res.json("User Result")
})

module.exports = router