const express = require("express");
const app = express();
const port = 3000;
const apiRoutes = require("./api.js");
const morgan = require("morgan");
const fs = require("fs");
const users = []


const errorHandler = (err, req, res, next) => {
    console.error()
    if(req.headers.accept == "application/json"){
        res.status(500).json({
            status: "fail",
            message: err.message
        })
    }
    else{
        next()
    }
}

const notFoundHandler = (req, res, next) => {
    res.status(404).json({
        status: "fail",
        message: "are you lost?"
    })
}

const handleLogin = (req, res) => {
    res.send("silahkan login")
}

app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(morgan("common"))
app.set("view engine", "ejs")

app.get("/", (req, res) => {
    res.render("index.ejs")
})

app.get("/game", (req, res) => {
    res.send("main game disini")
})

app.get("/login", handleLogin)

app.post("/login", (req, res) =>{
})

app.get("/greet", (req, res) => {
    const name = req.query.name || req.query.nama || "Stranger"
    res.render("greet.ejs", {
        name
    })
})

app.get("/register", (req, res) => {
    res.render("register")
})

app.post("/register", (req, res) => {
    const {email, password} = req.body
    
    let users = JSON.parse(fs.readFileSync("users.json", {encoding: "utf-8"}))
    users.push({email, password})
    fs.writeFileSync("users.json", JSON.stringify(users))
    console.log(users)
    res.redirect("/")
})

app.use("/api/user", apiRoutes)

app.use("/api/user/userCount", apiRoutes)

app.use(errorHandler)

app.use(notFoundHandler)

app.listen(port, () =>{
    console.log(`App listening on port ${port}`)
})